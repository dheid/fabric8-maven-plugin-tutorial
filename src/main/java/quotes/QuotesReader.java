package quotes;

import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Objects;

import static java.util.Objects.requireNonNull;

/**
 * Created by daniel on 07.07.17.
 */
@Component
public class QuotesReader {

    private static final String LINE_SEPARATOR = System.getProperty("line.separator");

    public Quotes read(URL url) {

        requireNonNull(url, "URL must not be null");

        Quotes quotes = new Quotes();

        try (InputStream inputStream = url.openStream();
             InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
             BufferedReader bufferedReader = new BufferedReader(inputStreamReader)) {

            final StringBuilder stringBuilder = new StringBuilder();

            bufferedReader.lines().forEach((line) -> {
                        if (Objects.equals(line, "%")) {
                            String content = stringBuilder.toString().trim();
                            Quote quote = new Quote();
                            quote.setContent(content);
                            quotes.add(quote);
                            stringBuilder.setLength(0);
                        } else {
                            stringBuilder.append(line);
                            stringBuilder.append(LINE_SEPARATOR);
                        }
                    }
            );
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }

        return quotes;
    }

}
