package quotes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by daniel on 07.07.17.
 */
@SpringBootApplication
public class QuotesApp {

    public static void main(String[] args) {
        SpringApplication.run(QuotesApp.class, args);
    }

}
