package quotes;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by daniel on 07.07.17.
 */
public class Quotes {

    private List<Quote> quotes = new ArrayList<>();

    private int numberOfQuotes;

    private Random random = new SecureRandom();

    public Quote getRandomQuote() {
        int randomIndex = random.nextInt(numberOfQuotes);
        return quotes.get(randomIndex);
    }

    public void add(Quote quote) {
        quotes.add(quote);
        numberOfQuotes++;
    }
}
