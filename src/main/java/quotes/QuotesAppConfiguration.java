package quotes;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by daniel on 07.07.17.
 */
@Configuration
public class QuotesAppConfiguration {

    @Autowired
    private QuotesFactory quotesFactory;

    @Bean
    public Quotes quotes() {
        return quotesFactory.produceQuotes();
    }

}
