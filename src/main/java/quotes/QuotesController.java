package quotes;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by daniel on 07.07.17.
 */
@RestController
public class QuotesController {

    @Autowired
    private Quotes quotes;

    @RequestMapping(value = "/", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<Quote> randomQuote() {
        return new ResponseEntity<>(quotes.getRandomQuote(), HttpStatus.OK);
    }

}
