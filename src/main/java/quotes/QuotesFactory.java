package quotes;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.net.URL;

/**
 * Created by daniel on 07.07.17.
 */
@Component
public class QuotesFactory {

    private final String resourceName;

    private final QuotesReader quotesReader;

    @Autowired
    public QuotesFactory(@Value("${quote.resource.name}") String resourceName, QuotesReader quotesReader) {
        this.resourceName = resourceName;
        this.quotesReader = quotesReader;
    }

    public Quotes produceQuotes() {
        URL resource = getClass().getResource(resourceName);
        return quotesReader.read(resource);
    }

}
