package quotes;

/**
 * Created by daniel on 07.07.17.
 */
public class Quote {

    private String content;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
