package quotes;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.net.URL;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

/**
 * Created by daniel on 07.07.17.
 */
@RunWith(MockitoJUnitRunner.class)
public class QuotesFactoryTest {

    @Mock
    private QuotesReader quotesReader;

    @Mock
    private Quotes quotes;

    @Test
    public void usesReaderToGetQuotes() throws Exception {

        given(quotesReader.read(any(URL.class))).willReturn(this.quotes);

        QuotesFactory quotesFactory = new QuotesFactory("/love", quotesReader);

        Quotes quotes = quotesFactory.produceQuotes();

        verify(quotesReader).read(any(URL.class));

        assertThat(quotes, is(this.quotes));

    }

}