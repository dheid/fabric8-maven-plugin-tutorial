package quotes;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.*;

/**
 * Created by daniel on 07.07.17.
 */
public class QuotesReaderTest {

    @Test
    public void readsQuotesFromResource() throws Exception {

        QuotesReader quotesReader = new QuotesReader();
        Quotes quotes = quotesReader.read(getClass().getResource("/love"));
        assertThat(quotes.getRandomQuote(), is(notNullValue()));

    }
}