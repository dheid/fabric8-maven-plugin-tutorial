# Little fabric8 Maven Plugin Tutorial

This tutorial allows you to implement the fabric8 Maven plugin into an existing Spring Boot application.

## Prerequisites

Please install the following components on your system to execute this example

- Java
- Maven
- Docker

## Usage

The master branch contains the initial setup of a Spring Boot application that returns random quotes every time
you access the URL

You may build it using

    mvn package

Then start it using

    mvn spring-boot:run

The application produces random quotes each time you access the URL

<http://localhost:8080/>

## Exercise

Instrument the fabric8 Maven plugin to wrap the Java process into a Docker container.

## Solution

You'll find a solution in the branch "result". Check it out using

    git checkout result